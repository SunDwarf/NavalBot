#!/usr/bin/env python3
import re
import requests

from bot.base import load

from flask import request
from flask import redirect

from app import app
from app import cfg
from app import version, versionappend


def get_external_ip():
    site = requests.get("http://checkip.dyndns.org/").content.decode()
    grab = re.findall('([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)', site)
    address = grab[0]
    return address


if __name__ == '__main__':

    load.load_all()

    @app.route('/')
    def navalbot():
        if request.environ.get('HTTP_X_REAL_IP', request.remote_addr) == "127.0.0.1" and not cfg.config.block_requests:
            return "NavalBot (" + get_external_ip() + ", {}{})".format(version, versionappend), 200, {"Content-Type": "text/plain"}
        else:
            return redirect("https://gitlab.com/SunDwarf/NavalBot", code=302)


    app.run(host="0.0.0.0")