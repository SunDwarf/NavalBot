from app import app
from app import versionappend, version
from pydiscourse import client as discourse_client

rule_dict = {}
module_dict = {}
web_modules = []


# Helper functions for getting things from the json responses

def get_topic_id(data):
    """
    Gets the topic ID from the dict provided.
    :param data: The dict
    :return: The ID, or None.
    """
    if 'topic_id' in data:
        return int(data['topic_id'])
    else:
        return None


def get_category(data):
    if 'category_id' in data:
        return int(data['category_id'])
    else:
        # Try and get the category ID via the topic.
        client = discourse_client.DiscourseClient()
        topic = client.topic(data['topic_id'])
        return int(topic['category_id'])


def get_groups(data):
    groups = []
    groups += ['trust_level_' + str(x) for x in range(0, data['trust_level'] + 1)]
    # Merge the custom groups list
    groups.append(data['custom_groups'])
    # Return the groups data
    if data['moderator']:
        groups.append('moderator')
    if data['admin']:
        groups.append('admin')
    return groups


def get_username(data):
    if 'username' in data:
        return data['username']
    else:
        return None


def invalid_command(user_data, post_data):
    if not app.config['RESPOND_INVALID']: return
    client = discourse_client.DiscourseClient()

    topic = get_topic_id(post_data)
    theiruser = get_username(user_data)
    # Construct a message.
    message = "Hi @{username}. You mentioned me, but gave an invalid or incorrectly formatted command.\n" \
              "I have been configured to respond to invalid mentions.\n" \
              "\n----------\n" \
              "**I am a bot, I will not respond to any messages.**\n" \
              "NavalBot can be found at [GitHub.](https://github.com/SunDwarf/NavalBot)".format(username=theiruser)
    client.create_post(message, topic)


def footer(post):
    return post + "\n----------\n"\
           "*NavalBot version {v}{ap}*.\n**I am a bot, I will not respond to any messages.**\n" \
                  "NavalBot can be found at [GitHub.](https://github.com/SunDwarf/NavalBot)".format(v=version, ap=versionappend)


def get_user_id(user_data):
    if 'id' in user_data:
        return user_data['id']
