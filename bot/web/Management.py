import json

from flask import blueprints, render_template, request, redirect
from flask.ext.login import login_required, logout_user

from bot.base import FlaskUsers

from app import app, cfg, cfg_forum
from bot import libnavalbot
from flask.ext import login as lgin
from bot.web.navalweb import status

import hashlib

management = blueprints.Blueprint("management", __name__)

def init():
    app.register_blueprint(management, url_prefix="/management")

def init_web():
    return "management.index", "Management"


@management.route("/api/reload", methods=["POST"])
@login_required
def reload():
    cfg_forum.reload()
    cfg.reload()
    return "", 200
    # TODO: Add more reload functions.
    # TODO: Add authentication.

@management.route("/api/status.json")
def route_status():
    s = status()
    if not s[0]:
        ob = {"status": 0, "msg": s[1], "warnings": []}
    else:
        ob = {"status": 1, "msg": "<h4>Warning! Your bot may be misconfigured.</h4>", "warnings": s[1]}
    return json.dumps(ob), 200, {"Content-Type": "text/json"}

@management.route("/api/login", methods=["POST"])
def login_api():
    u = FlaskUsers.get_user(request.form['username'])
    if u is None:
        return json.dumps({"status": 1, "msg": "Unknown username."}), 200, {"Content-Type": "text/json"}
    h = hashlib.new("sha512")
    h.update(request.form['password'].encode() + request.form['username'].encode())
    if h.hexdigest() == u.password:
        lgin.login_user(u)
        return json.dumps({"status": 0, "msg": "Login successful."})
    else:
        return json.dumps({"status": 1, "msg": "Invalid password."}), 200, {"Content-Type": "text/json"}

@management.route("/login/")
def login_render():
    return render_template("management/login.html")

@management.route('/logout/')
@login_required
def logout():
    logout_user()
    return redirect("/management/login", 302)

@management.route("/")
@login_required
def index():
    return render_template("management/index.html", modules=libnavalbot.module_dict,
                           rules=libnavalbot.rule_dict)
