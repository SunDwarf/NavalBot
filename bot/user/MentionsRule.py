import shlex

import bot.base.discourse.Decorators
from pydiscourse import client as discourse_client
from app import cfg
from .mentions import mentions as ment_cmds
from bot import libnavalbot


@bot.base.discourse.Decorators.PostCreatedRule
def mentions(user_data, post_data):
    """
    This is a handler method for mentions.
    Do not modify this function.
    """
    # Grab a client object.
    client = discourse_client.DiscourseClient()
    # Check if we're mentioned.
    is_mentioned = check_post_for_mention(post_data)
    # If we're not, exit.
    if not is_mentioned: return
    # Otherwise, loop through the commands and run them.
    msgs = get_msgs(post_data)
    # Otherwise, loop over the list of registered commands and run if it matches.
    for msg in msgs:
        gotcmd = False
        for func in ment_cmds:
            if func.__name__ == msg[0]:
                func(user_data, post_data, client, msg)
                gotcmd = True
                break
        if gotcmd:
            continue
        else:
            libnavalbot.invalid_command(user_data, post_data)


def check_post_for_mention(post_data):
    """
    Checks a post to see if it contains a mention.
    :param post_data: The post data.
    :return: If the user is mentioned.
    """
    post_content = post_data['raw']
    lines = [x for x in post_content.split('\n') if x != '']

    # Check if my name has been mentioned.
    username = cfg.config.username
    for line in lines:
        if '@{}'.format(username) in line:
            return True

def get_msgs(post_data):
    """
    Gets the command string for a mention command.
    """
    post_content = post_data['raw']
    lines = [x for x in post_content.split('\n') if x != '']

    msgs = []

    username = cfg.cfg.username
    for line in lines:
        if '@{}'.format(username) in line:
            # Partition the string.
            msg = line.partition('@{}'.format(username))[2].lstrip(' ').rstrip(' ')
            msg = shlex.split(msg)
            msgs.append(msg)

    return msgs
