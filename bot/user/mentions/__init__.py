# Add the files you wish to add here.
from .info import info
from .closethread import close
from .promote import grantforumadmin

from .GetSteamData import getsteamdata

# To add a new command to this list, simply add an import statement above, importing the sub into here.
# Then add it to the list below.

mentions = [
    # Bot information.
    info,
    close,
    grantforumadmin,
    getsteamdata
]
