import requests

from bot.base.discourse import Decorators
import pydiscourse.client
from bot import libnavalbot


def parse_rep_data(data):
    rep = data['reputation']
    if rep['summary'] == "none":
        return "No special tags."
    else:
        x = []
        for tag in rep['full'].split(','):
            x += tag.capitalize()
        return '\n' + '    '.join(x)


@Decorators.MentionRule
def getsteamdata(user_data, post_data, client_obj, msgdata):
    assert isinstance(client_obj, pydiscourse.client.DiscourseClient)
    if len(msgdata) < 2:
        # Get the username from the post.
        username = libnavalbot.get_username(user_data)
        # Get their custom field.
        steamid = client_obj.get_custom_field(username, 1)
    else:
        # Get the username from the msg.
        username = msgdata[1]
        steamid = client_obj.get_custom_field(username, 1)
    # Check steamid is not None
    if steamid is None:
        # bleh
        return
    steamid = steamid.replace(' ', '')
    # Calculate SteamID.
    try:
        alpha = steamid.split(':')[1]
        beta = steamid.split(':')[2]
        communityid = 76561197960265728 + int(alpha) + (int(beta) * 2)
    except:
        # Construct a message
        message = "Hi @{}. Unfortunately, I could not find the Steam2 ID "
    # Get the data from SteamREP's web api.
    data = requests.get("http://steamrep.com/api/beta3/reputation/{}".format(communityid),
                        params={"json": 1, "extended": 1}).json()['steamrep']
    print((data['vacban']))
    # Construct a message.
    message = "Steam data for user {u}:\n\n" \
    "**Community ID:** [{cid}](http://steamcommunity.com/id/{cid})\n" \
    "**Display name:** {dn}\n" \
    "**Trade banned:** {tb}\n" \
    "**VAC banned:** {vb}\n" \
    "**SteamREP Reputation:** {rep}".format(u=username, cid=communityid, dn=data['displayname'], tb=(True if data['tradeban'] != "1" else False), vb=True if data['vacban'] != "0" else False,
                               rep=parse_rep_data(data))
    message = libnavalbot.footer(message)
    topic = libnavalbot.get_topic_id(post_data)
    client_obj.create_post(message, topic)
