from bot import libnavalbot
from bot.base.discourse import Decorators


@Decorators.AuthenticatedMentionRule([])
def grantforumadmin(user_data, post_data, client_obj, msgdata):
    print("Granting forum admin...")
    if len(msgdata) < 2:
        libnavalbot.invalid_command(user_data, post_data)
    else:
        user = msgdata[1].replace('@', '')
        userobj = client_obj.user(user)
        userid = libnavalbot.get_user_id(userobj)
        client_obj._put("/admin/users/{}/grant_admin".format(userid))
        called_username = libnavalbot.get_username(user_data)
        topic = libnavalbot.get_topic_id(post_data)
        message = "Hi, @{}. The user @{} has been promoted to forum admin.\n" \
              "\n----------\n" \
              "**I am a bot, I will not respond to any messages.**\n" \
              "NavalBot can be found at [GitHub.](https://github.com/SunDwarf/NavalBot)".format(called_username, user)
        client_obj.create_post(message, topic)
