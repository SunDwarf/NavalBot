from bot import libnavalbot
from bot.base.discourse import Decorators


@Decorators.AuthenticatedMentionRule(["trust_level_4", "moderators"])
def info(user_data, post_data, client_obj, msgdata):
    topic = libnavalbot.get_topic_id(post_data)
    # Construct a message.
    message = "**NavalBot**, version *{}*, at your service.\n"
    message = libnavalbot.footer(message)
    client_obj.create_post(message, topic)
