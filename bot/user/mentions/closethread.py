from bot import libnavalbot
from bot.base.discourse.Decorators import AuthenticatedMentionRule


@AuthenticatedMentionRule(["moderators"])
def close(user_data, post_data, client_obj, msgdata):
    topic_id = libnavalbot.get_topic_id(post_data)
    client_obj.close(topic_id)

