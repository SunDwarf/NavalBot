from bot import libnavalbot
from bot.base.discourse import Decorators


@Decorators.CategoryTopicCreatedRule(13)
def introductions_created(user_data, post_data):
    userid = libnavalbot.get_user_id(user_data)
    username = libnavalbot.get_username(user_data)

    # Construct a message