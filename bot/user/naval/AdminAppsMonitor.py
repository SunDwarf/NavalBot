from bot.base.discourse import Decorators
import pydiscourse.client


@Decorators.CategoryTopicCreatedRule(14)
def admin_app_created(user_data, post_data):
    # Verify we're an admin application.
    print("Application posted, checking type...")
    if not 'Why we should consider you' in post_data['raw']:
        return
    # Get their steamID from their customfields
    client = pydiscourse.client.DiscourseClient()

