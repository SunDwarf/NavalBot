from flask.ext.login import UserMixin
from app import login_manager

from flask import redirect

from configmaster import YAMLConfigFile

import hashlib

class User(UserMixin):
    def __init__(self, user):
        super().__init__()
        self.id = user.id
        self.username = user.username
        self.password = user.password

def get_user(username):
    cfg = YAMLConfigFile.YAMLConfigFile("cfg/users.yml")
    pop = cfg.initial_populate({"users": {}})
    if pop: cfg.dump() and cfg.reload()

    for u in cfg.config.users:
        if u.username == username:
            return User(u)
    else:
        return None

@login_manager.user_loader
def user_callback(userid):
    cfg = YAMLConfigFile.YAMLConfigFile("cfg/users.yml")
    pop = cfg.initial_populate({"users": {}})
    if pop: cfg.dump() and cfg.reload()

    for u in cfg.config.users:
        if u.id == int(userid):
            return User(u)
    else:
        return None

@login_manager.unauthorized_handler
def unauthorized():
    return redirect("/management/login/", 302)
