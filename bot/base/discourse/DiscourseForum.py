import importlib
import json
import multiprocessing
import traceback
from flask import Blueprint, request
from app import cfg, cfg_forum, app
from bot import libnavalbot
import pydiscourse.client

post_rules = []
topic_rules = []

forum = Blueprint("navalbot", __name__)

def init():
    for rule in cfg_forum.config.rules:
        try:
            print("Loading rule {}".format(rule))
            x = importlib.import_module(rule)
            libnavalbot.rule_dict[rule] = x
        except Exception:
            print("Rule {} failed to load".format(rule))
            print("Stacktrace:")
            traceback.print_exc()

    app.register_blueprint(forum, url_prefix="/navalbot/endpoints")



@forum.route("/topic_created", methods=['POST'])
def topic_created():
    """
    This is called when a topic is created.
    Do not modify this. Instead, add a custom rule with the decorator BasicTopicCreated.{Category,Authenticated}TopicCreatedRule.
    :return: Nothing.
    """
    print("Topic created, handling data.")
    if validate_request(request.data.decode()):
        handle_rules(json.loads(request.data.decode())[1:], handle_topics)
    return ""


@forum.route("/post_created", methods=['POST'])
def post_created():
    """
    This is called when a post is created.
    Do not modify this. Instead, add a custom rule with the correct decorator.
    """
    print("Post created, handling data.")
    if validate_request(request.data.decode()):
        handle_rules(json.loads(request.data.decode())[1:], handle_posts)
    return ""


def handle_topics(user_data, post_data):
    for rule in topic_rules:
        try:
            rule(user_data, post_data)
        except Exception:
            if cfg.config.debug:
                exception = traceback.format_exc()
                print("Error happened handling topic.")
                print(exception)
                username = libnavalbot.get_username(user_data)
                topic = libnavalbot.get_topic_id(post_data)
                message = "Hi, @{}. Unfortunately your command failed with an error.\n" \
                          "Here's your stacktrace:\n" \
                          "```{}```\n" \
                          "\n----------\n" \
                          "**I am a bot, I will not respond to any messages.**\n" \
                          "NavalBot can be found at [GitHub.](https://github.com/SunDwarf/NavalBot)".format(
                    username, exception
                )
                message = libnavalbot.footer(message)
                client = pydiscourse.client.DiscourseClient()
                client.create_post(content=message, topic_id=topic)


def handle_posts(user_data, post_data):
    print("Handling post rule")
    for rule in post_rules:
        try:
            rule(user_data, post_data)
        except Exception:
            if cfg.config.debug:
                exception = traceback.format_exc()
                print("Error happened handling topic.")
                print(exception)
                username = libnavalbot.get_username(user_data)
                topic = libnavalbot.get_topic_id(post_data)
                message = "Hi, @{}. Unfortunately your command failed with an error.\n" \
                          "Here's your stacktrace:\n" \
                          "```{}```\n".format(
                    username, exception
                )
                message = libnavalbot.footer(message)
                client = pydiscourse.client.DiscourseClient()
                client.create_post(content=message, topic_id=topic)


def validate_request(data):
    secure = cfg.config.verify_requests
    jdata = json.loads(data)
    if not isinstance(jdata[0], str):
        print("WARNING: THE IDENTITY OF THIS REQUEST WAS INVALID. IGNORING.")
        return False
    else:
        api_key = jdata[0]
    if not secure:
        print("WARNING: THE IDENTITY OF THIS REQUEST COULD NOT BE VERIFIED.")
        return True
    else:
        return api_key == cfg.config.apikey


def handle_rules(data, callback):
    """
    Handles rules.

    This takes the FULL JSON data from a discourse web hook and calls a callback function.
    """
    # Get basic user data.
    basic_user_data = data[2]
    # Get post data
    post_data = data[0]
    # Check to see if the post is in the blacklist
    if libnavalbot.get_username(basic_user_data) in cfg.config.blacklist:
        return None
    # Run callback.
    # Launch a separate thread to handle the data.
    # NavalBot uses a separate thread to handle data because Discourse blocks on post/topic creating
    rule_handler = multiprocessing.Process(target=callback, args=(basic_user_data, post_data))
    rule_handler.start()
    return None