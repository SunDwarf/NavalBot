from bot import libnavalbot
from bot.base.discourse.DiscourseForum import post_rules, topic_rules

__author__ = 'eyes'


def PostCreatedRule(func):
    post_rules.append(func)

    def inner(user_data, post_data):
        func(user_data, post_data)
    inner.__name__ = func.__name__
    return inner

def AuthenticatedPostCreatedRule(groups: list):
    def real_decorator(func):
        def inner(user_data, post_data):
            user_groups = libnavalbot.get_groups(user_data)
            has_group = any(x in groups for x in groups)
            if has_group or 'admin' in user_groups:
                func(user_data, post_data)
        inner.__name__ = func.__name__
        post_rules.append(inner)
        return inner
    return real_decorator

def MentionRule(func):
    def inner(user_data, post_data, client_obj, msg_data):
        func(user_data, post_data, client_obj, msg_data)
    inner.__name__ = func.__name__
    return inner

def AuthenticatedMentionRule(groups: list):
    def real_decorator(func):
        def inner(user_data, post_data, client_obj, msg_data):
            user_groups = libnavalbot.get_groups(user_data)
            has_group = any(x in groups for x in groups)
            if has_group or 'admin' in user_groups:
                func(user_data, post_data, client_obj, msg_data)
        inner.__name__ = func.__name__
        return inner
    return real_decorator

def TopicCreatedRule(func):
    def inner(user_data, post_data):
        func(user_data, post_data)
    inner.__name__ = func.__name__
    topic_rules.append(inner)
    return inner

def CategoryTopicCreatedRule(category: int):
    def real_decorator(func):
        def inner(user_data, post_data):
            post_category = libnavalbot.get_category(post_data)
            if post_category == category:
                func(user_data, post_data)
        inner.__name__ = func.__name__
        topic_rules.append(inner)
        return inner
    return real_decorator
