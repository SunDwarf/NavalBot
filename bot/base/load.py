"""Loads all the modules from the configs."""

import importlib
import io
import sys
import traceback

import types

from app import cfg
from bot.libnavalbot import module_dict
from bot.web import navalweb


def load_modules():
    # Load modules.
    for module in cfg.config.modules:
        try:
            print("Loading module {}".format(module))
            x = importlib.import_module(module)
            if hasattr(x, "init"):
                x.init()
            module_dict[module] = x
        except Exception:
            print("Rule {} failed to load".format(module))
            print("Stacktrace:")
            traceback.print_exc()
        else:
            print("Loaded successfully.")

def load_all():
    load_modules()
    navalweb.init_web()

