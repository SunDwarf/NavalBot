import stripe
from app import app
import configmaster.YAMLConfigFile

from flask import request, render_template

cfg = configmaster.YAMLConfigFile.YAMLConfigFile("cfg/stripe.yml")
cfg.initial_populate({"stripe": {"secret_key": "yoursecretkey", "publish_key": "yourpublishkey"}})

stripe.api_key = cfg.config.stripe.secret_key



@app.route('/special/donate')
def donate_page():
    return render_template('naval/pay.html', key=cfg.config.stripe.publish_key)

@app.route("/navalbot/api/donate", methods=['POST'])
def donate():
    print(request.form)
    amount = 500
    print(request.form)
    customer = stripe.Customer.create(
        email='customer@example.com',
        card=request.form['stripeToken']
    )

    charge = stripe.Charge.create(
        customer=customer.id,
        amount=amount,
        currency='gbp',
        description='Flask Charge'
    )

    return render_template('naval/charge.html', amount=amount)
