import os
from app import app
from flask import send_from_directory, request
import zipfile
import traceback

import pathlib

authkeys = [
    "1DDC7B318B7D2B",
    "172F7F6022C953",
    "7725A50177F4F"
]

def generate_zip(server):
    if not os.path.exists('zips'):
        os.makedirs('zips')
    print("Generating zipfile for server", server)
    # Create a new zipfile.
    z = zipfile.ZipFile("zips/{}.zip".format(server), mode='w')
    # Add server files.
    print("Adding server files.")
    for root, subdir, files in os.walk("files/servers/{}/".format(server)):
        newroot = pathlib.PurePath(root).relative_to("files/servers/{}/".format(server))
        for file in files:
            print("Adding file {}/{}".format(root, file))
            z.write(root + "/" + file, arcname="{}/{}".format(str(newroot), '/' + file))
    # Get a list of files added.
    names = z.namelist()
    # These files are the same for all servers, and do not change.

    print("Adding binaries.")
    for root, subdir, files in os.walk("files/root/bin/"):
        newroot = pathlib.PurePath(root).relative_to("files/root/bin")
        for file in files:
            if "addons/sourcemod/bin/{}/{}".format(newroot, file) in names:
                continue
            z.write(root + "/" + file, arcname="addons/sourcemod/bin/{}/{}".format(str(newroot), '/' + file))

    print("Adding configs.")
    for root, subdir, files in os.walk("files/root/configs/"):
        newroot = pathlib.PurePath(root).relative_to("files/root/configs")
        for file in files:
            if "addons/sourcemod/configs/{}/{}".format(newroot, file) in names:
                continue
            z.write(root + "/" + file, arcname="addons/sourcemod/configs/{}/{}".format(str(newroot), '/' + file))
    print("Adding plugins.")
    for root, subdir, files in os.walk("files/root/plugins/"):
        newroot = pathlib.PurePath(root).relative_to("files/root/plugins")
        for file in files:
            if "addons/sourcemod/plugins/{}/{}".format(newroot, file) in names:
                continue
            z.write(root + "/" + file, arcname="addons/sourcemod/plugins/{}/{}".format(str(newroot), '/' + file))
    print("Adding extensions.")
    for root, subdir, files in os.walk("files/root/extensions/"):
        newroot = pathlib.PurePath(root).relative_to("files/root/extensions")
        for file in files:
            if "addons/sourcemod/extensions/{}/{}".format(newroot, file) in names:
                continue
            z.write(root + "/" + file, arcname="addons/sourcemod/extensions/{}/{}".format(str(newroot), '/' + file))
    print("Adding data.")
    for root, subdir, files in os.walk("files/root/data/"):
        newroot = pathlib.PurePath(root).relative_to("files/root/data")
        for file in files:
            if "addons/sourcemod/data/{}/{}".format(newroot, file) in names:
                continue
            z.write(root + "/" + file, arcname="addons/sourcemod/data/{}/{}".format(str(newroot), '/' + file))
    # Add translations.
    print("Adding translations.")
    for root, subdir, files in os.walk("files/root/translations"):
        newroot = pathlib.PurePath(root).relative_to("files/root/translations")
        for file in files:
            if "addons/sourcemod/translations/{}/{}".format(newroot, file) in names:
                continue
            z.write(root + "/" + file, arcname="addons/sourcemod/translations/{}/{}".format(str(newroot), file))
    print("Adding gamedata.")
    for root, subdir, files in os.walk("files/root/gamedata"):
        newroot = pathlib.PurePath(root).relative_to("files/root/gamedata")
        for file in files:
            if "addons/sourcemod/translations/{}/{}".format(newroot, file) in names:
                continue
            z.write(root + "/" + file, arcname="addons/sourcemod/gamedata/{}/{}".format(str(newroot), file))
    # Add server root configs.
    print("Adding root server configs.")
    for root, subdir, files in os.walk("files/root/sourcemod_configs/"):
        for file in files:
            if "cfg/sourcemod/{}".format(file) in names:
                continue
            z.write(root + "/" + file, arcname="cfg/sourcemod/{}".format(file))
    z.close()
    print("Done.")
    return "{}.zip".format(server)


@app.route("/getzip")
def getzip():
    server = request.args.get("server")
    auth = request.args.get("key")
    if auth in authkeys:
        if server:
            try:
                path = generate_zip(server)
                return send_from_directory("zips", path), 200, {
                    "Content-Disposition": "inline; filename=\"{}.zip\"".format(server)}
            except:
                traceback.print_exc()
                return "Internal error", 500
        else:
            return "Unknown server", 404
    else:
        return "Invalid key",


