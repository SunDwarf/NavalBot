from flask import Flask

from configmaster.YAMLConfigFile import YAMLConfigFile

from flask.ext import login

version = "0.2.1"  # Do not change.
versionappend = "-alpha"

app = Flask(__name__)

cfg_forum = YAMLConfigFile("cfg/navalbot.forum.yml")
populated = cfg_forum.initial_populate({
    "username": "myusername",
    "host": "myhost",
    "apikey": "myapikey",
    "respond_invalid": False,
    "verify_requests": True,
    "rules": [],
    "blacklist": []
})

# Add globals to jinja_env
app.jinja_env.globals.update(version=version, vappend=versionappend)

if populated: cfg_forum.dump() and cfg_forum.reload()

cfg = YAMLConfigFile("cfg/navalbot.yml")
populated = cfg.initial_populate({
    "debug": False,
    "modules": [],
    "block_requests": True,
    "redis_host": "localhost",
    "redis_post": 6379,
    "info_page": False,
    "about_page": True,
    "secret_key": False
})

if populated:
    cfg.dump() and cfg.reload()

if cfg.config.debug is True:
    app.config['DEBUG'] = True

app.config['SECRET_KEY'] = cfg.config.secret_key

login_manager = login.LoginManager()
login_manager.init_app(app)

