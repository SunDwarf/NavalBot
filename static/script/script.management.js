// Shitty noob javascript.
function reload_div() {
    $("#warning-misconfigured").slideUp();
    $.post("/management/api/cookie/reload");
    $.getJSON("/management/api/cookie/status.json", function(data) {
        $("#warning-misconfigured").html('<button id="reload-configure-alert" ' +
            'type="button" ' +
            'class="btn btn-xs pull-right btn-primary btn-material-green" ' +
            'onclick="reload_div();" style="background-color: #997852">' +
            'Reload' +
            '</button>');
        if (data['status'] === 0) {
            $("#warning-misconfigured").slideDown().addClass("alert-success").removeClass("alert-warning")
                .append(data['msg'])
        } else {
            $("#warning-misconfigured").slideDown().addClass("alert-warning").removeClass("alert-success")
                .append(data['msg']).append("<ul>");
            $.each(data['warnings'], function(i, item) {
               $("#warning-misconfigured").append("<li>" + item + "</li>");
            })
            $("#warning-misconfigured").append("</ul>")
        }
    })
}

function login() {
    $("#naval-login-error").slideUp();
    $.post("/management/api/cookie/login", $("#naval-login").serialize(), function(data, textStatus) {
        if (data['status'] === 1) {
            $("#naval-login-error").slideDown()
                .html('<h4>Oops!<h4>').append(data['msg']);
        } else {
            window.location.replace("/management/");
        }
    });
}
